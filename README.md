The Solution
============

This is the solution to the Business Card OCR challenge.  A description of the
challenge is included below.

Two solutions are provided to this challenge:
* python
* golang

The python solution is provided as it is one of the required languages.  For a
command line tool such as this, python checks many of the boxes.  I have quite
a bit of experience with ruby, but not a lot with python.  However, the languages
share many traits.  While I'm not a pythonista at this time, I'm always up
for learning new things.

The golang solution is also provided.  It has been my primary language for the
past few years.  golang is ideal for this kind of thing, as it creates a self
contained binary which is easy to give to someone to run.  With python and other
languages that require a vm and supporting modules, deployment can be an issue.
This is a primary reason many modern infrastructures and tools are being built
with golang, when they would have been built with python a few years ago.

In both cases, I have adapted the interfaces to be idiomatic for the language.

In both cases, start by cloning this repo:

```
    git clone https://bitbucket.org/tredeske/challenge.git
```

python
------

The python solution does not require any building, and is easy to run, provided
python is installed on your host.

As I'm not yet a pythonista, the testing and exception handling probably need
work, and I may not be completely following python idioms.

#### Setup

Setting up python is outside of the scope of this.  Consult your favorite
internet search tool if python is not setup for your environment.

#### Test
```
    cd challenge
    python test_units.py
```

#### Running

```
    cd challenge
    ./ocr.py -f [file]
```


golang
------

The golang solution must first be built.  You will need the golang tool set.
Once it is built, it is easy to run or copy it someplace and use it, as it
is a self contained executable.

#### Setup

Follow below to create `go`  and `gp` directories in your home account.
Choose the appropriate URL for your architecture from https://golang.org/dl/
This example uses the linux package, but windows and macOS are also available.
```
    cd
    wget https://dl.google.com/go/go1.11.4.linux-amd64.tar.gz
    tar xf go*.gz
    mkdir -p $HOME/gp
```

Update your environment (required for build only):
```
    PATH=$HOME/go/bin:$PATH
    export GOROOT=$HOME/go
    export GOPATH=$HOME/gp
```

#### Build and Test
```
    cd challenge
    go build
    go test ./
```

#### Running

```
    cd challenge
    ./challenge -file [file]
```



Business Card OCR Challenge
===========================

We’ve created a new smartphone app that enables users to snap a photo of a business card and have the information from the card automatically extracted and added to their contact list. We need you to write the component that parses the results of the optical character recognition (OCR) component in order to extract the name, phone number, and email address from the processed business card image.

For the challenge, we need you to build a command line tool or graphical user interface that takes the business card text as input and outputs the Name, Phone, and Email Address of the owner of the business card. We have provided you with an interface specification [1] that we’d like you to implement, as well as a series of example test cases [2].

### INTERFACE SPECIFICATION

```
ContactInfo
    String getName() : returns the full name of the individual (eg. John Smith, Susan Malick)
    String getPhoneNumber() : returns the phone number formatted as a sequence of digits
    String getEmailAddress() : returns the email address
```

```
BusinessCardParser
    ContactInfo getContactInfo(String document)
```

### EXAMPLE INPUT DOCUMENTS AND THEIR EXPECTED RESULTS

Example 1:

```
ASYMMETRIK LTD
Mike Smith
Senior Software Engineer
(410)555-1234
msmith@asymmetrik.com

==>

Name: Mike Smith
Phone: 4105551234
Email: msmith@asymmetrik.com
```

Example 2:

```
Foobar Technologies
Analytic Developer
Lisa Haung
1234 Sentry Road
Columbia, MD 12345
Phone: 410-555-1234
Fax: 410-555-4321
lisa.haung@foobartech.com

==>

Name: Lisa Haung
Phone: 4105551234
Email: lisa.haung@foobartech.com
```

Example 3:

```
Arthur Wilson
Software Engineer
Decision & Security Technologies
ABC Technologies
123 North 11th Street
Suite 229
Arlington, VA 22209
Tel: +1 (703) 555-1259
Fax: +1 (703) 555-1200
awilson@abctech.com

==>

Name: Arthur Wilson
Phone: 17035551259
Email: awilson@abctech.com
```
