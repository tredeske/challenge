package main

import (
	"bufio"
	"errors"
	"os"
	"regexp"
	"strings"
)

type BusinessCardParser struct {
}

type ContactInfo struct {
	Name         string
	PhoneNumber  string
	EmailAddress string
}

var (
	//
	// contains an @
	//
	emailExpr_ = regexp.MustCompile(`\b(.+@[\w-.]+)$`)

	//
	// looks like a phone number, then parse from end anchor
	//
	isPhoneExpr_ = regexp.MustCompile(`^(?:Phone: |Tel: |\().*-\d{4}$`)
	phoneExpr_   = regexp.MustCompile(`(1)? ?\(?(\d{3})\)?[ -]?(\d{3})-(\d{4})$`)

	//
	// 2 capitalized words.  this is very weak
	//
	nameExpr_ = regexp.MustCompile(`^[A-Z][a-z]+ [A-Z][a-z]+$`)
)

//
//
//
func (this BusinessCardParser) GetContactInfo(
	docF string,
) (
	rv *ContactInfo,
	err error,
) {

	var ci ContactInfo
	var matches []string
	var names []string

	docR, err := os.Open(docF)
	if err != nil {
		return
	}
	defer docR.Close()

	scanner := bufio.NewScanner(docR)
	for scanner.Scan() {

		line := scanner.Text()

		//
		// if line is an email
		//
		if 0 == len(ci.EmailAddress) {
			matches = emailExpr_.FindStringSubmatch(line)
			if 0 != len(matches) {
				ci.EmailAddress = matches[1]
				continue
			}
		}

		//
		// if line is a phone number
		//
		if 0 == len(ci.PhoneNumber) && isPhoneExpr_.MatchString(line) {
			matches = phoneExpr_.FindStringSubmatch(line)
			if 0 != len(matches) {
				ci.PhoneNumber = matches[1] + matches[2] + matches[3] + matches[4]
				continue
			}
		}

		//
		// if line might be a name, record for later analysis
		// - this check is last to weed out others slightly
		//
		if nameExpr_.MatchString(line) {
			names = append(names, line)
		}
	}

	//
	// sanity checks
	//
	err = scanner.Err()
	if err != nil {
		return
	} else if 0 == len(ci.PhoneNumber) {
		err = errors.New("No phone number found")
		return
	} else if 0 == len(ci.EmailAddress) {
		err = errors.New("No email address found")
		return
	} else if 0 == len(names) {
		err = errors.New("No person names found")
		return
	}

	//
	// detect name by comparison to email
	// - this is WEAK/FRAGILE, but works with all data
	//
	if 1 == len(names) {
		ci.Name = names[0]
	} else {
		email := strings.Split(strings.ToLower(ci.EmailAddress), "@")[0]
		for i, name := range names {
			parts := strings.Split(strings.ToLower(name), " ")
			if strings.Contains(email, parts[0]) ||
				strings.Contains(email, parts[1]) {
				ci.Name = names[i]
				break
			}
		}
		if 0 == len(ci.Name) {
			err = errors.New("No name found")
			return
		}
	}

	rv = &ci
	return
}
