package main

import (
	"flag"
	"fmt"
	"os"
)

func main() {

	var helpF bool
	var fileF string

	flag.BoolVar(&helpF, "help", helpF, "display help")
	flag.StringVar(&fileF, "file", "", "input filename")
	flag.Parse()

	if helpF {
		help()
	} else if 0 == len(fileF) {
		fmt.Printf("Must specify -file [FILE]\n")
		os.Exit(1)
	}

	ci, err := BusinessCardParser{}.GetContactInfo(fileF)
	if err != nil {
		fmt.Printf("Unable to parse %s: %s\n", fileF, err)
		os.Exit(1)
	}

	fmt.Printf("Name: %s\nPhone: %s\nEmail: %s\n",
		ci.Name, ci.PhoneNumber, ci.EmailAddress)

	os.Exit(0)
}

func help() {
	fmt.Printf(`
challenge -file FILE

Extract the Name, Email, and Phone from FILE.
`)
	os.Exit(0)
}
