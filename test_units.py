import unittest
import contact_info

class ContactInfoCase(unittest.TestCase):

    def test_parser(self):

	answers = [
            contact_info.ContactInfo(
                name = "Mike Smith",
                phone= "4105551234",
                email= "msmith@asymmetrik.com"
            ),
            contact_info.ContactInfo(
                name = "Lisa Haung",
                phone= "4105551234",
                email= "lisa.haung@foobartech.com",
            ),
            contact_info.ContactInfo(
                name = "Arthur Wilson",
                phone= "17035551259",
                email= "awilson@abctech.com",
            )
	]
        parser = contact_info.BusinessCardParser()

        for i in range(1,4):
            t = "case%d.txt" % i
            print t
            answer = answers[i-1]
            ci = parser.get_contact_info( t )
            self.assertEqual(ci.name, answer.name)
            self.assertEqual(ci.phone_number, answer.phone_number)
            self.assertEqual(ci.email_address, answer.email_address)

def main():
    unittest.main()

if __name__ == "__main__":
    main()


