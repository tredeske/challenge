package main

import (
	"fmt"
	"testing"
)

func TestParsing(t *testing.T) {

	answers := []ContactInfo{
		ContactInfo{
			Name:         "Mike Smith",
			PhoneNumber:  "4105551234",
			EmailAddress: "msmith@asymmetrik.com",
		},
		ContactInfo{
			Name:         "Lisa Haung",
			PhoneNumber:  "4105551234",
			EmailAddress: "lisa.haung@foobartech.com",
		},
		ContactInfo{
			Name:         "Arthur Wilson",
			PhoneNumber:  "17035551259",
			EmailAddress: "awilson@abctech.com",
		},
	}

	for i := 1; i < 4; i++ {
		file := fmt.Sprintf("case%d.txt", i)
		ci, err := BusinessCardParser{}.GetContactInfo(file)
		if err != nil {
			t.Fatalf("Unable to parse case %d: %s", i, err)
		}

		expect := &answers[i-1]
		if *ci != *expect {
			t.Fatalf("Did not get expected results: %#v != %#v", *ci, *expect)
		}
	}
}
