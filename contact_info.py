import re

class ContactInfo(object):
    name = ""
    phone_number = ""
    email_address = ""

    def __init__(self, name="", phone="", email=""):
        self.name = name
        self.phone_number = phone
        self.email_address = email

class BusinessCardParser(object):
    #
    # contains an @
    #
    email_expr = re.compile( r'\b(.+@[-\w.]+)$' )

    #
    # looks like a phone number, then parse from end anchor
    #
    is_phone_expr = re.compile( r'^(?:Phone: |Tel: |\().*-\d{4}$' )
    phone_expr    = re.compile( r'(1)? ?\(?(\d{3})\)?-? ?(\d{3})-(\d{4})$' )

    #
    # 2 capitalized words.  this is WEAK, but works with all known data
    #
    name_expr = re.compile( r'^[A-Z][a-z]+ [A-Z][a-z]+$' )

    #
    # Get contact info from input doc file
    #
    def get_contact_info( self, doc_f ):
        ci = ContactInfo()

        #
        # look through the input file for lines of interest
        #
        names = []
        with open( doc_f ) as f:
            while True:

                line = f.readline().strip()
                if not line:
                    break

                if "" == ci.email_address:
                    m = self.email_expr.search( line )
                    if m:
                        ci.email_address = m.group()
                        continue

                if "" == ci.phone_number and self.is_phone_expr.search(line):
                    m = self.phone_expr.search( line )
                    if m:
                        country = ''
                        if m.group(1):
                            country = m.group(1)
                        ci.phone_number = country + m.group(2) + m.group(3) + m.group(4)
                        continue

                #
                # if it looks like a person name, record it for later analysis
                #
                m = self.name_expr.search( line )
                if m:
                    names.append( m.group() )

        #
        # sanity checks
        #
        if "" == ci.phone_number:
            raise Exception("no phone number found")
        if "" == ci.email_address:
            raise Exception("no email address found")
        if 0 == len(names):
            raise Exception("no person names found")

        #
        # choose person name based on available ones by comparing with email
        # address.  we see if the 1st or last name is contained in the username
        # part of the email.
        #
        # this is WEAK, but works for all known data
        #
        if 1 == len(names):
            ci.name = names[0]
        else:
            email = ci.email_address.lower().split('@')[0]
            for name in names:
                parts = name.lower().split()
                if parts[0] in email or parts[1] in email:
                    ci.name = name
                    break
            if 0 == len(ci.name):
                raise Exception("no person name found")

        return ci
