#!/usr/bin/env python

import optparse
import sys
import os

this_d = os.path.abspath( os.path.dirname( sys.argv[0] ) )
sys.path.append( this_d )

import contact_info

#
# cmdline parsing
#

opt_parser = optparse.OptionParser()

opt_parser.add_option('-f', '--file',
    action="store", dest="file",
    help="filename to parse", default="")

cmd_options, args = opt_parser.parse_args()
if "" == cmd_options.file:
    print "add an -f option"
    exit(1)

#
# parse the card to get contact info
#

parser = contact_info.BusinessCardParser()

ci = parser.get_contact_info( cmd_options.file )

print "Name: " + ci.name
print "Phone: " + ci.phone_number
print "Email: " + ci.email_address

exit(0)
